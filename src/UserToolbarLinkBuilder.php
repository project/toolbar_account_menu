<?php

namespace Drupal\toolbar_account_menu;

use Drupal\Core\Menu\MenuTreeParameters;
use Drupal\user\ToolbarLinkBuilder;

/**
 * {@inheritDoc}
 */
class UserToolbarLinkBuilder extends ToolbarLinkBuilder {

  public function renderToolbarLinks(){
    $links = [];
    $parameters = new MenuTreeParameters();
    $parameters->maxDepth = 1;
    $parameters->onlyEnabledLinks();
    $menu = \Drupal::menuTree()->load('account',$parameters);

    \uasort($menu,function($l1, $l2){
      /** @var \Drupal\Core\Menu\MenuLinkTreeElement $l1 */
      /** @var \Drupal\Core\Menu\MenuLinkTreeElement $l2 */
      return $l1->link->getWeight() - $l2->link->getWeight();
    });

    //Recheck to make sure the links are enabled
    $menu = \array_filter($menu,function($link){
      /** @var \Drupal\Core\Menu\MenuLinkTreeElement $link */
      return $link->link->isEnabled();
    });

    /** @var \Drupal\Core\Menu\MenuLinkTreeElement $link */
    foreach($menu as $menu_link){
      $link = $menu_link->link;

      $links[$link->getPluginId()] = [
        'title' => $link->getTitle(),
        'url' => $link->getUrlObject(),
        // 'url' => Url::fromRoute('entity.user.edit_form', ['user' => $this->account->id()]),
        'attributes' => [
          'title' => $link->getTitle()
        ]
      ];
    }

    $build = [
      '#theme' => 'links__toolbar_user',
      '#links' => $links,
      '#attributes' => [
        'class' => ['toolbar-menu'],
      ],
      '#cache' => [
        'contexts' => ['user'],
      ],
    ];

    return $build;
  }

}
